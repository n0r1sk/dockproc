FROM alpine:latest

COPY dockproc /dockproc
RUN mkdir -p /host/proc && mkdir /host/docker && chmod +x /dockproc && mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

CMD ["./dockproc", "prometheus", "-p", "8080", "--hostsystem", "/host"]

EXPOSE 8080